const { users } = require("../models");
const db = require("../models");
const Users = db.users;
const AuthToken = db.authtokens;
var CryptoJS = require("crypto-js");



// Create and Save a new user
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
        res.status(400).send({ message: "Name can not be empty!" });
        return;
    }

    // Encrypt
    var encryptedPassword = CryptoJS.AES.encrypt(req.body.password, req.body.email).toString();

    // Create a user
    const users = new Users({
        name: req.body.name,
        email: req.body.email,
        password: encryptedPassword
    });

    // Save user in the database
    users.save(users)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user."
            });
        });
};

exports.login = (req, res) => {
    if (!req.body.email) {
        res.status(400).send({ message: "Email cannot be empty" })
        return
    }
    if (!req.body.password) {
        res.status(400).send({ message: "Password cannot be empty" })
        return
    }
    console.log(req.body)
    Users.findOne({
            email: req.body.email
        }).exec()
        .then((response) => {
            if (response) {
                console.log(response)
                    // Decrypt
                var bytes = CryptoJS.AES.decrypt(response.password, response.email);
                var originalPassword = bytes.toString(CryptoJS.enc.Utf8);
                if (req.body.password === originalPassword) {
                    let hasedToken = CryptoJS.lib.WordArray.random(128 / 8).toString();
                    // Create a user
                    const auth_token = new AuthToken({
                        user_id: response._id,
                        token: hasedToken,
                        login_time: new Date()
                    });

                    // Save user in the database
                    auth_token.save(auth_token)
                        .then(data => {
                            req.session.sessionId = hasedToken;
                            res.status(200).send({
                                token: hasedToken,
                                message: "Login Successfully"
                            });
                        })
                        .catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating the user."
                            });
                        });
                } else {
                    res.status(200).send({
                        message: "Password doesnot match. Please check the password"
                    })
                }
            } else {
                res.status(200).send({
                    message: "User doesnot exists. Please check the emailid"
                })
            }
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user."
            });
        });
}

exports.getUser = (req, res) => {
    if (req.headers['x-auth-token']) {
        console.log(req.session)
        if (req.session.sessionId == req.headers['x-auth-token']) {
            AuthToken.findOne({
                    token: req.headers['x-auth-token']
                }).exec()
                .then((authResposne) => {
                    if (authResposne) {
                        Users.findOne({
                                _id: authResposne.user_id
                            }).exec()
                            .then((userResonse) => {
                                if (userResonse) {
                                    res.status(200).send({
                                        message: "User details fetched successfully",
                                        data: userResonse
                                    })
                                } else {
                                    res.status(500).send({
                                        message: "Some error occurred while fetching the user."
                                    });
                                }
                            })
                            .catch(err => {
                                res.status(500).send({
                                    message: err.message || "Some error occurred while fetching the user."
                                });
                            });
                    } else {
                        res.status(500).send({
                            message: "Some error occurred while fetching the user."
                        });
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while fetching the user."
                    });
                });
        } else {
            res.status(500).send({
                message: "You are not an authorized user."
            })
        }
    } else {
        res.status(500).send({
            message: "You are not an authorized user."
        })
    }
}

exports.getUserList = (req, res) => {
    if (req.headers['x-auth-token']) {
        if (req.session.sessionId == req.headers['x-auth-token']) {
            console.log(req.session)
            AuthToken.findOne({
                    token: req.headers['x-auth-token']
                }).exec()
                .then((authResposne) => {
                    if (authResposne) {
                        AuthToken.aggregate([{
                                $lookup: {
                                    from: "users",
                                    localField: "user_id",
                                    foreignField: "_id",
                                    as: "userObj"
                                }
                            }, {
                                $unwind: "$userObj"
                            }, {
                                $project: {
                                    user: "$userObj",
                                    login_time: 1,
                                }
                            }]).exec()
                            .then((userResonse) => {
                                if (userResonse) {
                                    res.status(200).send({
                                        message: "Users details fetched successfully",
                                        data: userResonse
                                    })
                                } else {
                                    res.status(500).send({
                                        message: "Some error occurred while fetching the users."
                                    });
                                }
                            })
                            .catch(err => {
                                res.status(500).send({
                                    message: err.message || "Some error occurred while fetching the users."
                                });
                            });
                    } else {
                        res.status(500).send({
                            message: "Some error occurred while fetching the users."
                        });
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while fetching the users."
                    });
                });
        } else {
            res.status(500).send({
                message: "You are not an authorized user."
            })
        }
    } else {
        res.status(500).send({
            message: "You are not an authorized user."
        })
    }
}