module.exports = app => {
    const users = require("../controllers/user.controller.js");

    var router = require("express").Router();

    // Create a new user
    router.post("/", users.create);
    router.post("/login", users.login);
    router.get("/get-user", users.getUser);
    router.get("/get-users", users.getUserList)

    app.use('/api/users', router);
};