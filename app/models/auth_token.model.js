module.exports = mongoose => {
    const AuthToken = mongoose.model(
        "auth_tokens",
        mongoose.Schema({
            "token": String,
            "user_id": String,
            "login_time": Date,
            "logout_time": Date
        })
    );

    return AuthToken;
};